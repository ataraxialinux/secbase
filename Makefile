include config.mk

.SUFFIXES:
.SUFFIXES: .o .c

BIN = \
	getfattr \
	setfattr

OBJ = $(BIN:=.o)
SRC = $(BIN:=.c)

all: $(BIN)

install: $(BIN)
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f $(BIN) $(DESTDIR)$(PREFIX)/bin

clean: $(BIN)
	rm $(BIN)

$(BIN): $(@:=.o)

.o:
	$(CC) $(LDFLAGS) -o $@ $< $(LIB)

.c.o:
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ -c $<

.PHONY: all install clean

#include <sys/stat.h>
#include <sys/xattr.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static bool dumpOnlyByName;

static void usage(void)
{
	fprintf(stderr, "usage: getfattr [-n name] path...\n"
		"                -d path...\n");
	exit(1);
}

int main(int argc, char **argv)
{
	int c;
	ssize_t len = 0, keylen = 0, vallen = 0;
	char *name = NULL, *buf = NULL, *key = NULL, *path = NULL, *val = NULL;
	char *pretty_path = NULL;
	struct stat st;

	/* From Toybox */
	ssize_t (*getter)(const char *, const char *, void *, size_t) = getxattr;
	ssize_t (*lister)(const char *, char *, size_t) = listxattr;

	while ((c = getopt(argc, argv, "dn:")) != -1) {
		switch (c) {
			case 'd':
				dumpOnlyByName = false;
				break;
			case 'n':
				name = optarg;
				dumpOnlyByName = true;
				break;
			default:
				usage();
		}
	}

	argc -= optind;
	argv += optind;

	if (!argv[0])
		usage();

	buf = malloc(sizeof(char) * 1024);
	if (!buf) {
		perror("malloc");
		return 1;
	}

	for (int i = 0; i < argc; i++) {
		path = argv[i];
		pretty_path = argv[i];

		if (lstat(path, &st) == -1) {
			perror("lstat");
			return 1;
		}

		if (S_ISLNK(st.st_mode)) {
			getter = lgetxattr;
			lister = llistxattr;
		}

		if (dumpOnlyByName) {
			if (getter(path, name, buf, 1024) == -1)
				return -1;

			if (path[0] == '/') path++;

			printf("# file: %s\n", path);
			printf("%s=\"%s\"\n\n", name, buf);
		} else {
			/* Determine buffer len */
			len = lister(path, NULL, 0);
			if (len == -1)
				goto out;

			len = lister(path, buf, len);
			if (len == -1)
				goto out;

			if (pretty_path[0] == '/') pretty_path++;

			printf("# file: %s\n", pretty_path);

			key = buf;
			while (len > 0) {
				vallen = getxattr(path, key, NULL, 0);
				if (vallen == -1)
					goto out;

				if (vallen > 0) {
					val = malloc(vallen + 1);
					if (!val) {
						perror("malloc");
						return 1;
					}

					vallen = getxattr(path, key, val, vallen);
					if (vallen == -1)
						goto out;

					printf("%s=\"%s\"\n", key, val);

					free(val);
				} else if (vallen == 0) {
					printf("%s=\"<no value>\"\n", key);
				}

				keylen = strlen(key) + 1;
				len -= keylen;
				key += keylen;
			}

			printf("\n");
		}
	}

	free(buf);

	return 0;

out:
	perror("getfattr");
	free(buf);
	return 1;
}

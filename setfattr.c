#include <sys/stat.h>
#include <sys/xattr.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static bool attrRemove;

static void usage(void)
{
	fprintf(stderr, "usage: setfattr [-n name] [-v value] path...\n"
		"                [-x name] path...\n");
	exit(1);
}

int main(int argc, char **argv)
{
	int c;
	char *name = NULL, *value = NULL, *remove = NULL, *path = NULL;
	struct stat st;

	/* Inspired by Toybox */
	int (*setter)(const char *, const char *, const void *, size_t, int) = setxattr;
	int (*remover)(const char *, const char *) = removexattr;

	while ((c = getopt(argc, argv, "n:v:x:")) != -1) {
		switch (c) {
			case 'n':
				name = optarg;
				attrRemove = false;
				break;
			case 'v':
				value = optarg;
				break;
			case 'x':
				remove = optarg;
				attrRemove = true;
				break;
			default:
				usage();
		}
	}

	argc -= optind;
	argv += optind;

	path = argv[0];

	if (!path)
		usage();

	if (lstat(path, &st) == -1) {
		perror("lstat");
		return 1;
	}

	if (S_ISLNK(st.st_mode)) {
		setter = lsetxattr;
		remover = lremovexattr;
	}

	if (attrRemove) {
		if (remover(path, remove) == -1)
			goto out;
	} else {
		if (setter(path, name, value, strlen(value), 0) == -1)
			goto out;
	}

	return 0;

out:
	perror("setfattr");
	return 1;
}
